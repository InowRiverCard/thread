package com.threadjava.mailService;

import com.threadjava.mailService.dto.SharePostDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 14.06.2020
 */
@RestController
@RequestMapping("/api/share-post")
public class SharePostController {

    @Autowired
    MailNotificationService mailNotificationService;

    @PostMapping
    public Boolean sendMessage(@RequestBody SharePostDto sharePostDto) {
        mailNotificationService.sendShareNotification(
                sharePostDto.getUserId(),
                sharePostDto.getPostId().toString(),
                sharePostDto.getRecipientMail()
        );
        return true;
    }
}
