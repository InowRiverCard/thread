package com.threadjava.resetPwd;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 21.06.2020
 */
public class ResetPasswordException extends RuntimeException {
    ResetPasswordException(String msg) {
        super(msg);
    }
}
