package com.threadjava.resetPwd.model;

import com.threadjava.db.BaseEntity;
import com.threadjava.users.models.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 10.06.2020
 */

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ResetPasswordToken extends BaseEntity {

    private String token;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    private Boolean isDeleted;
}