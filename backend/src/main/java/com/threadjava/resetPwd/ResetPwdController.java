package com.threadjava.resetPwd;

import com.threadjava.auth.dto.UserLoginErrorDto;
import com.threadjava.resetPwd.dto.ResetPwdDto;
import com.threadjava.resetPwd.dto.ResetPwdRequestDto;
import com.threadjava.resetPwd.dto.ResetPwdResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 03.06.2020
 */
@RestController
@RequestMapping("/api/reset-pwd")
public class ResetPwdController {

    private final ResetPwdService resetPwdService;

    public ResetPwdController(ResetPwdService resetPwdService) {
        this.resetPwdService = resetPwdService;
    }

    @PostMapping("/request")
    public ResponseEntity<ResetPwdResponseDto> resetPwdRequest(@RequestBody ResetPwdRequestDto resetPwdRequestDto) {
        ResponseEntity<ResetPwdResponseDto> result;
        try {
            var responseDto = resetPwdService.resetPasswordRequest(resetPwdRequestDto.getEmail());
            result = ResponseEntity.ok(responseDto);
        } catch (ResetPasswordException e) {
            var error = new ResetPwdResponseDto(e.getMessage());
            result = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
        }
        return result;
    }

    @PostMapping
    public ResponseEntity<ResetPwdResponseDto> resetPassword(@RequestBody ResetPwdDto resetPwdDto) {
        ResponseEntity<ResetPwdResponseDto> result;
        try {
            var responseDto = resetPwdService.resetPassword(resetPwdDto);
            result = ResponseEntity.ok(responseDto);
        } catch (ResetPasswordException e) {
            var error = new ResetPwdResponseDto(e.getMessage());
            result = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
        }
        return result;
    }
}
