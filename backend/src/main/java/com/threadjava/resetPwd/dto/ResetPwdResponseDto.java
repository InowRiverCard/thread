package com.threadjava.resetPwd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 10.06.2020
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResetPwdResponseDto {
    String message;
}
