package com.threadjava.resetPwd.dto;

import lombok.Data;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 12.06.2020
 */
@Data
public class ResetPwdRequestDto {
    String email;
}
