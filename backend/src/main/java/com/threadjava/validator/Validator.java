package com.threadjava.validator;

import com.threadjava.auth.dto.UserLoginDTO;
import com.threadjava.auth.dto.UserLoginErrorDto;
import com.threadjava.auth.dto.UserRegisterDto;
import com.threadjava.auth.dto.UserRegisterErrorDto;
import com.threadjava.users.UsersRepository;
import com.threadjava.users.dto.UserFieldUpdResponseDto;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 19.06.2020
 */
@Service
public class Validator {

    private static final String NOT_EMPTY_EMAIL = "Email can`t be empty";
    private static final String INVALID_EMAIL = "Email should be be valid";
    private static final String EXISTING_EMAIL= "Such email already exists";
    private static final String NOT_EMPTY_NAME = "Name can`t be empty";
    private static final String SHORT_NAME = "Name can`t be shorter then 4 characters";
    private static final String EXISTING_NAME= "This name already busy";
    private static final String NOT_EMPTY_PASSWORD = "Password can`t be empty";
    private static final String SHORT_PASSWORD = "Password can`t be shorter then 4 characters";

    private final UsersRepository usersRepository;

    public Validator(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public Optional<String> validateEmail(String email) {
        var emailValidator = EmailValidator.getInstance();
        Optional<String> result = Optional.empty();

        if (email == null) {
            result = Optional.of(NOT_EMPTY_EMAIL);
        } else if (!emailValidator.isValid(email)) {
            result = Optional.of(INVALID_EMAIL);
        } else if (usersRepository.findByEmail(email).isPresent()) {
            result = Optional.of(EXISTING_EMAIL);
        }

        return result;
    }

    public Optional<String> validateEmailForResetPwd(String email) {
        var emailValidator = EmailValidator.getInstance();
        Optional<String> result = Optional.empty();
        if (email == null) {
            result = Optional.of(NOT_EMPTY_EMAIL);
        } else if (!emailValidator.isValid(email)) {
            result = Optional.of(INVALID_EMAIL);
        }
        return result;
    }

    public Optional<String> validateName(String name) {
        Optional<String> result = Optional.empty();

        if (name.isEmpty()) {
            result = Optional.of(NOT_EMPTY_NAME);
        } else if (name.length() < 4) {
            result = Optional.of(SHORT_NAME);
        } else if (usersRepository.findByUsername(name).isPresent()) {
            result = Optional.of(EXISTING_NAME);
        }

        return result;
    }

    public Optional<String> validatePassword(String pwd) {
        Optional<String> result = Optional.empty();
        if (pwd.isEmpty()) {
            result = Optional.of(NOT_EMPTY_PASSWORD);
        } else if (pwd.length() < 4) {
            result = Optional.of(SHORT_PASSWORD);
        }

        return result;
    }

    public Optional<UserRegisterErrorDto> validateUserRegisterForm(UserRegisterDto user) {
        Optional<UserRegisterErrorDto> result = Optional.empty();
        var errorDto = new UserRegisterErrorDto();
        var emailError = validateEmail(user.getEmail());
        var nameError = validateName(user.getUsername());
        var password = validatePassword(user.getPassword());

        if (emailError.isPresent() || nameError.isPresent() || password.isPresent()) {
            emailError.ifPresent(errorDto::setEmail);
            nameError.ifPresent(errorDto::setUsername);
            password.ifPresent(errorDto::setPassword);
            result = Optional.of(errorDto);
        }

        return result;
    }

}
