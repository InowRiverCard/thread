package com.threadjava.comment;

import com.threadjava.comment.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;
import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentCreationResponseDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        return commentService.create(commentDto);
    }

    @PutMapping
    public  CommentCreationResponseDto edit(@RequestBody CommentEditionDto editedDto) {
        editedDto.setUserId(getUserId());
        var item = commentService.update(editedDto);
        template.convertAndSend("/topic/edit_comment", item);
        return item;
    }

    @DeleteMapping
    public CommentDeletionResponseDto delete(@RequestBody CommentEditionDto deletedDto) {
        deletedDto.setUserId(getUserId());
        var item = commentService.delete(deletedDto);
        template.convertAndSend("/topic/delete", item);
        return item;
    }
}
