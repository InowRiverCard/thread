package com.threadjava.comment;

import com.threadjava.comment.dto.*;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.PostsRepository;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private PostsRepository postsRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findCommentById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentCreationResponseDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentCreationResponseDto(postCreated);
    }

    public CommentCreationResponseDto update(CommentEditionDto commentDto) {
        Comment post = CommentMapper.MAPPER.commentEditDtoToModel(commentDto);
        Comment postUpdated = commentRepository.save(post);
        return CommentMapper.MAPPER.commentToCommentCreationResponseDto(postUpdated);
    }

    public CommentDeletionResponseDto delete(CommentEditionDto commentDto) {
        Comment post = CommentMapper.MAPPER.commentEditDtoToModel(commentDto);
        post.setIsDeleted(true);
        Comment postUpdated = commentRepository.save(post);
        return CommentMapper.MAPPER.commentToPostDeletionResponseDto(postUpdated);
    }
}
