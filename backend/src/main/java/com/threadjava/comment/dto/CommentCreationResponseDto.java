package com.threadjava.comment.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CommentCreationResponseDto {
    private UUID id;
    private UUID postId;
    private UUID userId;
}
