package com.threadjava.comment.dto;

import lombok.Data;

import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 31.05.2020
 */
@Data
public class CommentDeletionResponseDto {
    private UUID commentId;
    private UUID postId;
}
