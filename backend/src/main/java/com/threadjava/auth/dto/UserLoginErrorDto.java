package com.threadjava.auth.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 20.06.2020
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserLoginErrorDto {
    private String email;
    private String password;
}
