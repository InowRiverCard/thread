package com.threadjava.auth;

import com.threadjava.auth.dto.*;
import com.threadjava.users.UsersService;
import com.threadjava.users.dto.UserFieldUpdResponseDto;
import com.threadjava.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final AuthService authService;

    private final Validator validator;

    public AuthController(AuthService authService, Validator validator) {
        this.authService = authService;
        this.validator = validator;
    }

    @PostMapping("/register")
    public ResponseEntity signUp(@RequestBody UserRegisterDto user) throws Exception {
        ResponseEntity result;
        var validationErrors = validator.validateUserRegisterForm(user);
        if (validationErrors.isPresent()) {
            result = ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(validationErrors.get());
        } else {
            result = ResponseEntity.ok(authService.register(user));
        }
        
        return result;
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody UserLoginDTO user) throws Exception {
        ResponseEntity result;
        try {
            result = ResponseEntity.ok(authService.login(user));
        } catch (Exception e) {
            var error = new UserLoginErrorDto(null, e.getMessage());
            result = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
        }
        return result;
    }
}
