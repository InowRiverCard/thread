package com.threadjava.postReactions;

import com.threadjava.events.dto.Notification;
import com.threadjava.mailService.MailNotificationService;
import com.threadjava.post.PostsService;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {
    private final PostReactionService postReactionService;
    private final SimpMessagingTemplate template;
    private final PostsService postsService;
    private final MailNotificationService mailNotification;

    @Autowired
    public PostReactionController(PostReactionService postReactionService,
                                  SimpMessagingTemplate template,
                                  PostsService postsService,
                                  MailNotificationService mailNotification) {
        this.postReactionService = postReactionService;
        this.template = template;
        this.postsService = postsService;
        this.mailNotification = mailNotification;
    }

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction){
        postReaction.setUserId(getUserId());
        var reaction = postReactionService.setReaction(postReaction);
        var postAuthorId = postsService.getPostAuthorId(postReaction.getPostId());
        if (reaction.isPresent() && !postAuthorId.equals(reaction.get().getUserId())) {
            this.setMailNotification(reaction.get());
            this.setNotification(postAuthorId.toString());
        }
        return reaction;
    }

    private void setMailNotification(ResponsePostReactionDto reaction) {
        mailNotification.sendLikePostNotification(reaction.getUserId(), reaction.getPostId());
    }

    private void setNotification(String authorId) {
        var destinationLink = "/queue/" + authorId + "/like";
        template.convertAndSend(destinationLink, new Notification("Your post was liked!"));
    }

}
