package com.threadjava.users;

import com.threadjava.image.dto.ImageDto;
import com.threadjava.users.dto.*;
import com.threadjava.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final UsersService userDetailsService;

    @Autowired
    private Validator validator;

    public UserController(UsersService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }

    @PutMapping("/update_email/{userId}")
    public ResponseEntity<UserFieldUpdResponseDto> changeEmail(@PathVariable UUID userId,
                                                               @RequestBody UserFieldUpdDto email) {
        ResponseEntity<UserFieldUpdResponseDto> result;
        var error = validator.validateEmail(email.getValue());
        if (error.isPresent()) {
            result = ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new UserFieldUpdResponseDto(error.get()));
        } else {
            var updatedEmail = userDetailsService.updateEmail(userId, email.getValue());
            result = ResponseEntity.ok(updatedEmail);
        }
        return result;

    }

    @PutMapping("/update_name/{userId}")
    public ResponseEntity<UserFieldUpdResponseDto> changeName(@PathVariable UUID userId,
                                                              @RequestBody UserFieldUpdDto name) {
        ResponseEntity<UserFieldUpdResponseDto> result;
        var error = validator.validateName(name.getValue());
        if (error.isPresent()) {
            result = ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new UserFieldUpdResponseDto(error.get()));
        } else {
            var updatedEmail = userDetailsService.updateName(userId, name.getValue());
            result = ResponseEntity.ok(updatedEmail);
        }
        return result;
    }

    @PutMapping("/update_image/{userId}")
    public UserDetailsDto setUserAvatar(@PathVariable UUID userId, @RequestBody ImageDto image) {
        return userDetailsService.updateAvatar(userId, image);
    }

    @PutMapping("/update_status/{userId}")
    public ResponseEntity setUserStatus(@PathVariable UUID userId, @RequestBody UserStatusDto status) {
        var result = userDetailsService.updateStatus(userId, status);
        return ResponseEntity.ok(result);
    }

}
