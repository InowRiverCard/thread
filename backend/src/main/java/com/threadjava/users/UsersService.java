package com.threadjava.users;

import com.threadjava.auth.model.AuthUser;
import com.threadjava.image.ImageRepository;
import com.threadjava.image.dto.ImageDto;
import com.threadjava.users.dto.*;
import com.threadjava.users.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.lang.module.FindException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UsersService implements UserDetailsService {

    private final UsersRepository usersRepository;

    private final ImageRepository imageRepository;

    public UsersService(UsersRepository usersRepository, ImageRepository imageRepository) {
        this.usersRepository = usersRepository;
        this.imageRepository = imageRepository;
    }

    @Override
    public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
        return usersRepository
                .findByEmail(email)
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public UserDetailsDto getUserById(UUID id) {
        return usersRepository
                .findById(id)
                .map(UserMapper.MAPPER::userToUserDetailsDto)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public List<UserReactedResponseDto> getPostReactionUserList(UUID postId, boolean isLike) {
        return usersRepository
                .getPostReactionUserList(postId, isLike)
                .stream()
                .map(UserMapper.MAPPER::userQueryResultToUserLikedResponse)
                .collect(Collectors.toList());
    }

    public List<UserReactedResponseDto> getCommentReactionUserList(UUID commentId, boolean isLike) {
        return usersRepository
                .getCommentReactionUserList(commentId, isLike)
                .stream()
                .map(UserMapper.MAPPER::userQueryResultToUserLikedResponse)
                .collect(Collectors.toList());
    }

    public void save(User user) {
        usersRepository.save(user);
    }

    public UserDetailsDto updateAvatar(UUID userId, ImageDto avatar) {
        var user = this.getUser(userId);
        var image = imageRepository
                .findById(avatar.getId())
                .orElseThrow(() -> new FindException("No image found with id"));
        user.setAvatar(image);
        var result = usersRepository.save(user);
        return UserMapper.MAPPER.userToUserDetailsDto(result);
    }

    public UserDetailsDto updateStatus(UUID userId, UserStatusDto status) {
        var user = this.getUser(userId);
        user.setStatus(status.getStatus());
        var result = usersRepository.save(user);
        return UserMapper.MAPPER.userToUserDetailsDto(result);
    }

    public UserFieldUpdResponseDto updateEmail(UUID userId, String email) {
        var result = new UserFieldUpdResponseDto();
        var userWithSuchEmail = usersRepository.findByEmail(email);
        if (userWithSuchEmail.isPresent() && !userWithSuchEmail.get().getId().equals(userId)) {
            throw new UserDuplicateException("Such email already exists");
        } else {
            var updatedUser = this.getUser(userId);
            updatedUser.setEmail(email);
            usersRepository.save(updatedUser);
            result.setValue(updatedUser.getEmail());
        }
        return result;
    }

    public UserFieldUpdResponseDto updateName(UUID userId, String name) {
        var result = new UserFieldUpdResponseDto();
        var userWithSuchName = usersRepository.findByUsername(name);
        if (userWithSuchName.isPresent() && !userWithSuchName.get().getId().equals(userId)) {
            throw new UserDuplicateException("This name already busy");
        } else {
            var updatedUser = this.getUser(userId);
            updatedUser.setUsername(name);
            usersRepository.save(updatedUser);
            result.setValue(updatedUser.getUsername());
        }
        return result;
    }

    private User getUser(UUID id) {
        return usersRepository
                .findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with id"));
    }

}