package com.threadjava.users.dto;

import com.threadjava.image.dto.ImageDto;
import lombok.Data;

import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 03.06.2020
 */
@Data
public class UserReactedResponseDto {
    UUID id;
    private String username;
    private ImageDto image;
}
