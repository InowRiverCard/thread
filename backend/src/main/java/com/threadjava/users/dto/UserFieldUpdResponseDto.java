package com.threadjava.users.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 17.06.2020
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserFieldUpdResponseDto {
    private String value;
}
