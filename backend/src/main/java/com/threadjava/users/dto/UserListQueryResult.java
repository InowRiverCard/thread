package com.threadjava.users.dto;

import com.threadjava.image.model.Image;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 03.06.2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserListQueryResult {
    private UUID id;
    private String username;
    private Image image;
}
