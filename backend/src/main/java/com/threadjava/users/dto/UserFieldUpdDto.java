package com.threadjava.users.dto;

import lombok.Data;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 17.06.2020
 */
@Data
public class UserFieldUpdDto {
    private String value;
}
