package com.threadjava.users.dto;

import lombok.Data;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 08.06.2020
 */
@Data
public class UserStatusDto {
    private String status;
}
