package com.threadjava.users;

import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserReactedResponseDto;
import com.threadjava.users.dto.UserListQueryResult;
import com.threadjava.users.models.User;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper MAPPER = Mappers.getMapper( UserMapper.class );

    @Mapping(source = "avatar", target = "image")
    UserDetailsDto userToUserDetailsDto(User user);


    UserReactedResponseDto userQueryResultToUserLikedResponse(UserListQueryResult userQueryResult);
}
