package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue="0") Integer from,
                                 @RequestParam(defaultValue="10") Integer count,
                                 @RequestParam(required = false) UUID userId,
                                 @RequestParam(required = false) String ownPostsMode,
                                 @RequestParam(required = false) String likedPostsMode) {
        return postsService.getAllPosts(from, count, userId, ownPostsMode, likedPostsMode);
    }



    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @PutMapping
    public  PostCreationResponseDto edit(@RequestBody PostEditionDto editedDto) {
        editedDto.setUserId(getUserId());
        var item = postsService.update(editedDto);
        var destinationLink = "/queue/" + item.getUserId().toString() + "/edit_post";
        template.convertAndSend(destinationLink, item);
        return item;
    }

    @DeleteMapping
    public PostDeletionResponseDto delete(@RequestBody PostEditionDto deletedDto) {
        deletedDto.setUserId(getUserId());
        var item = postsService.delete(deletedDto);
        var destinationLink = "/queue/" + item.getUserId().toString() + "/deleted";
        template.convertAndSend(destinationLink, item);
        return item;
    }


}
