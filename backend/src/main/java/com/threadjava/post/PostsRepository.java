package com.threadjava.post;

import com.threadjava.post.dto.PostDetailsQueryResult;
import com.threadjava.post.model.Post;
import com.threadjava.post.dto.PostListQueryResult;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostsRepository extends JpaRepository<Post, UUID> {

    @Query("SELECT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments c WHERE c.isDeleted = FALSE ), " +
            "p.createdAt, i, p.user, p.isDeleted) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE ((:ownPostsMode is null ) " +
            "   OR (p.user.id = :userId AND :ownPostsMode = 'show') " +
            "   OR (p.user.id != :userId AND :ownPostsMode = 'hide')) " +
            "AND (:likeMode is null " +
            "   OR :likeMode = 'liked' " +
            "   AND((SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) " +
            "       FROM p.reactions pr WHERE (pr.post = p AND pr.user.id = :userId)) != 0)) " +
            "AND (p.isDeleted is false) " +
            "order by p.createdAt desc" )
    List<PostListQueryResult> findAllPosts(@Param("userId") UUID userId,
                                           @Param("ownPostsMode") String ownPostsMode,
                                           @Param("likeMode") String likeMode,
                                           Pageable pageable);


    @Query("SELECT new com.threadjava.post.dto.PostDetailsQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments c WHERE c.isDeleted = FALSE), " +
            "p.createdAt, p.updatedAt, i, p.user, p.isDeleted) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE p.id = :id")
    Optional<PostDetailsQueryResult> findPostById(@Param("id") UUID id);
}