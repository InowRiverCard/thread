import React from 'react';
import PropTypes from 'prop-types';
import { Popup } from 'semantic-ui-react';
import UserListItem from '../PopupUserListItem';

const UserPopup = ({ btn, userList, setUserList, id, disabled }) => {
  const loadLikersList = () => {
    setUserList(id, true);
  };

  const getContent = () => (
    userList
      ? <div>{userList.map(u => <UserListItem user={u} key={u.id} />)}</div>
      : null
  );

  const clearLikersList = () => {
    setUserList(id, false);
  };

  const likeTrigger = btn();

  return (
    <Popup
      hoverable
      disabled={disabled}
      trigger={likeTrigger}
      content={getContent}
      on="hover"
      open={Boolean(userList)}
      onClose={clearLikersList}
      onOpen={loadLikersList}
      position="top left"
    />
  );
};

UserPopup.propTypes = {
  userList: PropTypes.arrayOf(PropTypes.any),
  id: PropTypes.string.isRequired,
  setUserList: PropTypes.func.isRequired,
  btn: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired
};

UserPopup.defaultProps = {
  userList: undefined
};

export default UserPopup;
