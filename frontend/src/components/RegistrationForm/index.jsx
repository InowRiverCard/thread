import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import validator from 'validator';
import {
  Form,
  Button,
  Segment
} from 'semantic-ui-react';

const RegistrationForm = ({ isAuthorized, registration, errors, setValidationErrors }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [isLoading, setLoading] = useState(false);

  const emailChanged = value => {
    setEmail(value);
    const updError = ({
      ...errors,
      email: undefined
    });
    setValidationErrors(updError);
  };

  const usernameChanged = value => {
    setUsername(value);
    const updError = ({
      ...errors,
      username: undefined
    });
    setValidationErrors(updError);
  };

  const passwordChanged = value => {
    setPassword(value);
    const updError = ({
      ...errors,
      password: undefined
    });
    setValidationErrors(updError);
  };

  const validateEmail = () => {
    if (!validator.isEmail(email)) {
      const updError = ({
        ...errors,
        email: 'Please enter a valid Email'
      });
      setValidationErrors(updError);
    }
  };

  const validateName = () => {
    if (!username) {
      const updError = ({
        ...errors,
        username: 'this field is required'
      });
      setValidationErrors(updError);
    }
  };

  const validatePwd = () => {
    if (!password) {
      const updError = ({
        ...errors,
        password: 'this field is required'
      });
      setValidationErrors(updError);
    }
  };

  const validateInputData = () => {
    validateEmail();
    validateName();
    validatePwd();
  };

  const isEmpty = obj => (Object.entries(obj).length === 0);

  const register = async () => {
    validateInputData();
    const isValid = isEmpty(errors);
    if (!isValid || isLoading) {
      return;
    }
    setLoading(true);
    try {
      await registration({ email, password, username });
    } finally {
      setLoading(false);
    }
  };

  return isAuthorized
    ? <Redirect to="/" />
    : (
      <Form name="registrationForm" size="large" onSubmit={register}>
        <Segment>
          <Form.Field>
            <Form.Input
              fluid
              icon="at"
              iconPosition="left"
              placeholder="Email"
              type="email"
              error={errors.email}
              onChange={ev => emailChanged(ev.target.value)}
              onBlur={validateEmail}
            />
          </Form.Field>
          <Form.Field>
            <Form.Input
              fluid
              icon="user"
              iconPosition="left"
              placeholder="Username"
              type="text"
              error={errors.username}
              onChange={ev => usernameChanged(ev.target.value)}
              onBlur={validateName}
            />
          </Form.Field>
          <Form.Field>
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              onChange={ev => passwordChanged(ev.target.value)}
              error={errors.password}
              onBlur={validatePwd}
            />
          </Form.Field>
          <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
            Register
          </Button>
        </Segment>
      </Form>

    );
};

RegistrationForm.propTypes = {
  isAuthorized: PropTypes.bool,
  registration: PropTypes.func.isRequired,
  setValidationErrors: PropTypes.func.isRequired,
  errors: PropTypes.objectOf(PropTypes.any).isRequired
};

RegistrationForm.defaultProps = {
  isAuthorized: undefined
};

export default RegistrationForm;
