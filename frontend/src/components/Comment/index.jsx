import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import UserPopup from '../UserPopup/index';
import styles from './styles.module.scss';

const Comment = ({
  comment,
  likeComment,
  dislikeComment,
  callDeleteCommentModal,
  editCommentModal,
  postId,
  userId,
  setCommentLikedUserList: setLikerList,
  setCommentDislikedUserList: setDislikerList
}) => {
  const { id, body, createdAt, user, dislikeCount, likeCount, likerList, dislikerList } = comment;
  const editedCommentDto = {
    commentId: id,
    postId,
    body
  };

  const likeBtn = () => (
    <CommentUI.Action onClick={() => likeComment(id)}>
      <Icon name="thumbs up" />
      {likeCount}
    </CommentUI.Action>
  );

  const dislikeBtn = () => (
    <CommentUI.Action onClick={() => dislikeComment(id)}>
      <Icon name="thumbs down" />
      {dislikeCount}
    </CommentUI.Action>
  );

  const editBtn = () => {
    let result = null;
    if (userId === user.id) {
      result = (
        <CommentUI.Action>
          <Icon name="edit" onClick={() => editCommentModal(editedCommentDto)} />
        </CommentUI.Action>
      );
    }
    return result;
  };

  const deleteBtn = () => {
    let result = null;
    if (userId === user.id) {
      result = (
        <CommentUI.Action>
          <Icon name="trash" onClick={() => callDeleteCommentModal(editedCommentDto)} />
        </CommentUI.Action>
      );
    }
    return result;
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar as="a" src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author>
          {user.username}
          <CommentUI.Metadata>
            {moment(createdAt).fromNow()}
          </CommentUI.Metadata>
        </CommentUI.Author>
        <CommentUI.Metadata>
          <p>{user.status}</p>
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <UserPopup
            btn={likeBtn}
            userList={likerList}
            setUserList={setLikerList}
            id={id}
            disabled={Number(likeCount) === 0}
          />
          <UserPopup
            btn={dislikeBtn}
            userList={dislikerList}
            setUserList={setDislikerList}
            id={id}
            disabled={dislikeCount === 0}
          />
          {editBtn()}
          {deleteBtn()}
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  userId: PropTypes.string.isRequired,
  postId: PropTypes.string.isRequired,
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  callDeleteCommentModal: PropTypes.func.isRequired,
  editCommentModal: PropTypes.func.isRequired,
  setCommentLikedUserList: PropTypes.func.isRequired,
  setCommentDislikedUserList: PropTypes.func.isRequired
};

export default Comment;
