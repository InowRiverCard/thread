import React, { useState } from 'react';
import { Form, Button, Modal, Label, Icon, Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const UpdateUserPropertyModal = ({
  header,
  setProperty,
  type,
  defaultValue,
  validationError,
  setError,
  setUpdateSuccess,
  propertyName,
  isSuccess
}) => {
  const [value, setValue] = useState(defaultValue);
  const [isLoading, setIsLoading] = useState(false);
  const [open, setOpen] = useState(false);

  const valueChanged = data => {
    setValue(data);
    setError(undefined);
  };

  const handleSaveClick = async () => {
    if (defaultValue === value) {
      return;
    }

    setIsLoading(true);
    try {
      await setProperty({ value });
    } finally {
      setIsLoading(false);
    }
  };

  const close = () => {
    setOpen(false);
    setUpdateSuccess({ isSuccess: undefined });
  };

  const sendBtn = {
    color: 'olive',
    content: 'Send',
    disabled: isLoading
  };

  const closeBtn = {
    color: 'olive',
    content: 'Ok',
    labelPosition: 'right',
    icon: 'checkmark',
    onClick: () => close()
  };

  const btn = (
    <Button icon floated="left" onClick={() => setOpen(true)}>
      <Icon name="edit" />
    </Button>
  );

  return (
    <Modal trigger={btn} open={open}>
      <Modal.Header>{header}</Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleSaveClick}>
          <Form.Field>
            <Form.Input
              type={type}
              loading={isLoading}
              error={Boolean(validationError)}
              value={value}
              onChange={ev => valueChanged(ev.target.value)}
              action={isSuccess ? closeBtn : sendBtn}
            />
            {validationError && (
              <Label pointing prompt>
                {validationError}
              </Label>
            )}
          </Form.Field>
        </Form>
        {isSuccess && (
          <Message positive>
            You succesfully changed your
            {propertyName}
          </Message>
        )}
      </Modal.Content>
      {!isSuccess && (
        <Modal.Actions>
          <Button color="red" onClick={() => close()}>Cancel</Button>
        </Modal.Actions>
      )}
    </Modal>
  );
};

UpdateUserPropertyModal.defaultProps = {
  validationError: undefined,
  isSuccess: undefined
};

UpdateUserPropertyModal.propTypes = {
  header: PropTypes.string.isRequired,
  setProperty: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  defaultValue: PropTypes.string.isRequired,
  validationError: PropTypes.string,
  setError: PropTypes.func.isRequired,
  setUpdateSuccess: PropTypes.func.isRequired,
  propertyName: PropTypes.string.isRequired,
  isSuccess: PropTypes.string
};

export default UpdateUserPropertyModal;
