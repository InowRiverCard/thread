import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Modal, Message, Label } from 'semantic-ui-react';

import styles from './styles.module.scss';

const ForgotPwdModal = ({ resetPwdRequest, setResetPasswordMessage, responseAnswer }) => {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [validMessege, setValidMessege] = useState(undefined);
  const [open, setOpen] = useState(false);

  const closeBtnColor = () => (responseAnswer.success ? 'teal' : 'red');

  const toggleModal = () => {
    setOpen(!open);
    setResetPasswordMessage({ success: undefined });
  };

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
    setValidMessege(undefined);
    setResetPasswordMessage({ error: undefined });
  };

  const validateEmail = async () => {
    const isEmpty = validator.isEmpty(email);
    if (isEmpty) {
      setIsEmailValid(false);
      setValidMessege('Please Fill in this field');
      return false;
    }
    const isValid = validator.isEmail(email);
    if (!isValid) {
      setIsEmailValid(false);
      setValidMessege('Please enter valid email');
      return false;
    }
    return true;
  };

  const handleLoginClick = async () => {
    const isValid = await validateEmail();
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await resetPwdRequest({ email });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Modal size="tiny" open={open} trigger={<Button basic color="blue" onClick={toggleModal} content="Reset" />}>
      <Modal.Header className={styles.header}>
        <span>Reset Password</span>
      </Modal.Header>
      <Modal.Content>
        <Form name="loginForm" size="large">
          <Form.Field>
            <Form.Input
              fluid
              icon="at"
              iconPosition="left"
              placeholder="Email"
              type="email"
              loading={isLoading}
              disabled={isLoading}
              error={!isEmailValid}
              onChange={ev => emailChanged(ev.target.value)}
              onBlur={validateEmail}
            />
            {validMessege && <Label basic color="red" pointing>{validMessege}</Label>}
          </Form.Field>
        </Form>
        { responseAnswer.success && <Message positive>{responseAnswer.success}</Message> }
        { responseAnswer.error && <Message negative>{responseAnswer.error}</Message> }
      </Modal.Content>
      <Modal.Actions>
        <Button color={closeBtnColor()} content="Close" onClick={toggleModal} />
        { !responseAnswer.success && (
          <Button
            onClick={handleLoginClick}
            color="teal"
            disabled={isLoading}
            content="Send"
          />
        )}
      </Modal.Actions>
    </Modal>
  );
};

ForgotPwdModal.propTypes = {
  resetPwdRequest: PropTypes.func.isRequired,
  setResetPasswordMessage: PropTypes.func.isRequired,
  responseAnswer: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ForgotPwdModal;
