import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Input, Button } from 'semantic-ui-react';
import { setUserStatus } from '../../containers/Profile/actions';
import styles from './styles.module.scss';

const StatusEditionModal = ({ currentStatus, close, setUserStatus: updateStatus }) => {
  const [status, setStatus] = useState(currentStatus);

  const updateStatusBody = e => {
    let text = e.target.value;
    if (text.length > 65) {
      text = text.substring(0, 65);
    }
    setStatus(text);
  };

  const handleSaveBtn = () => {
    updateStatus({ status });
    close();
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Status</span>
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          value={status}
          onChange={updateStatusBody}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button color="teal" onClick={handleSaveBtn} content="Save" />
      </Modal.Actions>
    </Modal>
  );
};

StatusEditionModal.propTypes = {
  currentStatus: PropTypes.string,
  close: PropTypes.func.isRequired,
  setUserStatus: PropTypes.func.isRequired
};

StatusEditionModal.defaultProps = {
  currentStatus: undefined
};

const actions = { setUserStatus };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(StatusEditionModal);
