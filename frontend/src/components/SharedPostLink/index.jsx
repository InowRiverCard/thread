import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Modal, Input, Icon } from 'semantic-ui-react';
import validator from 'validator';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close, sendMail, userId }) => {
  const [copied, setCopied] = useState(false);
  const [email, setEmail] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(true);
  let input = useRef();

  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied(true);
  };

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleSendBtn = () => {
    setIsEmailValid(validator.isEmail(email));
    if (!isEmailValid) {
      return;
    }
    try {
      sendMail({ userId, recipientMail: email, postId });
    } catch {
      // do something
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => { input = ref; }}
        />
        <Input
          fluid
          placeholder="Email"
          type="email"
          action={{
            color: 'olive',
            labelPosition: 'right',
            icon: 'mail',
            content: 'Send',
            onClick: handleSendBtn
          }}
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  sendMail: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

export default SharedPostLink;
