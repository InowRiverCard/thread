import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const EditComment = ({ editingObj, close, editFunc }) => {
  const [body, setBody] = useState(editingObj.body);

  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    const updated = { ...editingObj, body };
    await editFunc(updated);
    close();
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Comment</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleAddPost}>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="Enter your text"
            onChange={ev => setBody(ev.target.value)}
          />
          <Button floated="right" color="blue" type="submit" disabled={body === ''}>Post</Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

EditComment.propTypes = {
  editingObj: PropTypes.objectOf(PropTypes.any).isRequired,
  editFunc: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditComment;
