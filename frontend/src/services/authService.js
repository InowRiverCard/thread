import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const resetPasswordRequest = async email => {
  const response = await callWebApi({
    endpoint: '/api/reset-pwd/request',
    type: 'POST',
    request: email
  });
  return response.json();
};

export const resetPassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/reset-pwd',
    type: 'POST',
    request
  });
  return response.json();
};
