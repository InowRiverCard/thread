import callWebApi from 'src/helpers/webApiHelper';

export const getLikedPostUserList = async postId => {
  const response = await callWebApi({
    endpoint: `/api/users/liked_post/${postId}`,
    type: 'GET'
  });
  return response.json();
};

export const getdislikedPostUserList = async postId => {
  const response = await callWebApi({
    endpoint: `/api/users/disliked_post/${postId}`,
    type: 'GET'
  });
  return response.json();
};

export const getLikedCommentUserList = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/users/liked_comment/${commentId}`,
    type: 'GET'
  });
  return response.json();
};

export const getCommentDislikedUserList = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/users/disliked_comment/${commentId}`,
    type: 'GET'
  });
  return response.json();
};

export const updateUserAvatar = async (userId, image) => {
  const response = await callWebApi({
    endpoint: `/api/user/update_image/${userId}`,
    type: 'PUT',
    request: image
  });
  return response.json();
};

export const updateUserStatus = async (userId, status) => {
  const response = await callWebApi({
    endpoint: `/api/user/update_status/${userId}`,
    type: 'PUT',
    request: status
  });
  return response.json();
};

export const updateUserEmail = async (userId, email) => {
  const response = await callWebApi({
    endpoint: `/api/user/update_email/${userId}`,
    type: 'PUT',
    request: email
  });
  return response.json();
};

export const updateUserName = async (userId, name) => {
  const response = await callWebApi({
    endpoint: `/api/user/update_name/${userId}`,
    type: 'PUT',
    request: name
  });
  return response.json();
};

