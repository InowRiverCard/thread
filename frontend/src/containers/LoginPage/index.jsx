/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  login,
  setValidationErrors,
  resetPasswordRequest,
  setResetPasswordMessage
} from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import LoginForm from 'src/components/LoginForm';
import ForgotPwdModal from '../../components/ForgotPwdModal';

const LoginPage = ({
  login: signIn,
  setValidationErrors: setErrors,
  validationErrors,
  resetPasswordRequest: resetPwdRequest,
  setResetPasswordMessage: setResetPasswordMsg,
  resetPasswordMessge
}) => {
  const [resetPwdModal, ToggleResetPwd] = useState(false);

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Login to your account
        </Header>
        <LoginForm login={signIn} errors={validationErrors} setValidationErrors={setErrors} />
        <Message>
          New to us?
          {' '}
          <NavLink exact to="/registration">Sign Up</NavLink>
        </Message>
        <Message>
          forgot password?
          {' '}
          <ForgotPwdModal
            setResetPasswordMessage={setResetPasswordMsg}
            resetPwdRequest={resetPwdRequest}
            responseAnswer={resetPasswordMessge}
          />
        </Message>
      </Grid.Column>
    </Grid>
  );
};

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  setValidationErrors: PropTypes.func.isRequired,
  validationErrors: PropTypes.objectOf(PropTypes.any),
  resetPasswordMessge: PropTypes.objectOf(PropTypes.any),
  resetPasswordRequest: PropTypes.func.isRequired,
  setResetPasswordMessage: PropTypes.func.isRequired
};

LoginPage.defaultProps = {
  validationErrors: {},
  resetPasswordMessge: {}
};

const mapStateToProps = rootState => ({
  validationErrors: rootState.profile.validationErrors,
  resetPasswordMessge: rootState.profile.resetPasswordMessge
});

const actions = { login, setValidationErrors, resetPasswordRequest, setResetPasswordMessage };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
