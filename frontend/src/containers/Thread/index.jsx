/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import EditComment from 'src/components/EditComment';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import ConfirmationWindow from 'src/components/DeleteConfirmationModal';
import SharedPostLink from 'src/components/SharedPostLink';
import EditPostForm from 'src/components/EditPost';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  editPost,
  deletePost,
  sharePostByEmail,
  setPostLikedUserList,
  setPostDislikedUserList
} from './postActions';
import {
  deleteComment,
  editComment
} from './commentActions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  ownPostsMode: undefined,
  likedPostsMode: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  editPost: edit,
  likePost: like,
  dislikePost: dislike,
  deletePost: remove,
  deleteComment: removeComment,
  editComment: editCommentFunc,
  toggleExpandedPost: toggle,
  sharePostByEmail: shareByEmail,
  setPostLikedUserList: setPostLikerList,
  setPostDislikedUserList: setPostDislikerList
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [editingPost, setEditedPost] = useState(undefined);
  const [deletingPost, setDeletingPost] = useState(undefined);
  const [deletingComment, setDeletingComment] = useState(undefined);
  const [editingComment, setEditingComment] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [onlyLikedPosts, setOnlyLikedPosts] = useState(false);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    if (!onlyLikedPosts) {
      postsFilter.userId = hideOwnPosts ? undefined : userId;
    }

    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.ownPostsMode = showOwnPosts ? undefined : 'show';
    if (!showOwnPosts && hideOwnPosts) {
      setHideOwnPosts(false);
    }
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    if (!onlyLikedPosts) {
      postsFilter.userId = hideOwnPosts ? undefined : userId;
    }

    postsFilter.ownPostsMode = hideOwnPosts ? undefined : 'hide';
    if (!hideOwnPosts && showOwnPosts) {
      setShowOwnPosts(false);
    }

    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleOnlyLikedPosts = () => {
    setOnlyLikedPosts(!onlyLikedPosts);
    if (!hideOwnPosts && !showOwnPosts) {
      postsFilter.userId = hideOwnPosts ? undefined : userId;
    }
    postsFilter.likedPostsMode = onlyLikedPosts ? undefined : 'liked';
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const callDeleteCommentConfirmModal = comment => {
    setDeletingComment(comment);
  };

  const callEditCommentModal = comment => {
    setEditingComment(comment);
  };

  const callDeleteConfirmModal = post => {
    setDeletingPost(post);
  };

  const callEditForm = post => {
    setEditedPost(post);
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        {' '}
        <Checkbox
          toggle
          label="Hide my posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only liked posts"
          checked={onlyLikedPosts}
          onChange={toggleOnlyLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            callEditModal={callEditForm}
            callDeleteConfirm={callDeleteConfirmModal}
            userId={userId}
            key={post.id}
            setPostLikedUserList={setPostLikerList}
            setPostDislikedUserList={setPostDislikerList}
          />
        ))}
      </InfiniteScroll>
      {expandedPost
        && (
          <ExpandedPost
            sharePost={sharePost}
            updatePost={callEditForm}
            callDeleteConfirm={callDeleteConfirmModal}
            editCommentModal={callEditCommentModal}
            deleteCommentConfirm={callDeleteCommentConfirmModal}
          />
        )}
      {sharedPostId
        && (
          <SharedPostLink
            postId={sharedPostId}
            close={() => setSharedPostId(undefined)}
            sendMail={shareByEmail}
            userId={userId}
          />
        )}
      {editingPost
        && (
          <EditPostForm
            post={editingPost}
            close={() => setEditedPost(undefined)}
            editPost={edit}
            uploadImage={uploadImage}
          />
        )}
      {deletingPost
        && (
          <ConfirmationWindow
            deletedObj={deletingPost}
            close={() => setDeletingPost(undefined)}
            deleteFunc={remove}
            message="Delete Post"
          />
        )}
      {deletingComment
        && (
          <ConfirmationWindow
            deletedObj={deletingComment}
            close={() => setDeletingComment(undefined)}
            deleteFunc={removeComment}
            message="Delete Comment"
          />
        )}
      {editingComment
        && (
          <EditComment
            editingObj={editingComment}
            close={() => setEditingComment(undefined)}
            editFunc={editCommentFunc}
          />
        )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  sharePostByEmail: PropTypes.func.isRequired,
  setPostLikedUserList: PropTypes.func.isRequired,
  setPostDislikedUserList: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  editPost,
  toggleExpandedPost,
  addPost,
  deletePost,
  deleteComment,
  editComment,
  sharePostByEmail,
  setPostLikedUserList,
  setPostDislikedUserList
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
