import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  toggleExpandedPost,
  dislikePost,
  setPostLikedUserList,
  setPostDislikedUserList
} from 'src/containers/Thread/postActions';
import {
  addComment,
  dislikeComment,
  likeComment,
  setCommentLikedUserList,
  setCommentDislikedUserList
} from 'src/containers/Thread/commentActions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  updatePost: update,
  addComment: add,
  callDeleteConfirm: remove,
  userId,
  deleteCommentConfirm,
  editCommentModal,
  dislikeComment: dislikeCom,
  likeComment: likeCom,
  setCommentLikedUserList: setCommentLikerList,
  setCommentDislikedUserList: setCommentHaterList,
  setPostLikedUserList: setPostLikerList,
  setPostDislikedUserList: setPostHaterList
}) => (
  <Modal centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            callEditModal={update}
            sharePost={sharePost}
            callDeleteConfirm={remove}
            userId={userId}
            setPostLikedUserList={setPostLikerList}
            setPostDislikedUserList={setPostHaterList}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  comment={comment}
                  likeComment={likeCom}
                  dislikeComment={dislikeCom}
                  editCommentModal={editCommentModal}
                  callDeleteCommentModal={deleteCommentConfirm}
                  postId={post.id}
                  userId={userId}
                  setCommentLikedUserList={setCommentLikerList}
                  setCommentDislikedUserList={setCommentHaterList}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  callDeleteConfirm: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  deleteCommentConfirm: PropTypes.func.isRequired,
  editCommentModal: PropTypes.func.isRequired,
  setCommentLikedUserList: PropTypes.func.isRequired,
  setCommentDislikedUserList: PropTypes.func.isRequired,
  setPostLikedUserList: PropTypes.func.isRequired,
  setPostDislikedUserList: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  likePost,
  toggleExpandedPost,
  addComment,
  dislikePost,
  likeComment,
  dislikeComment,
  setCommentLikedUserList,
  setCommentDislikedUserList,
  setPostLikedUserList,
  setPostDislikedUserList
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
