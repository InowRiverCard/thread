import * as authService from 'src/services/authService';
import * as userService from 'src/services/usersServise';
import {
  SET_USER,
  SET_VALIDATION_ERRORS,
  SET_RESET_PWD_MESSAGE,
  SET_IS_UPDATE_SUCCESS
} from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

export const setUpdateSuccess = isSuccess => async dispatch => dispatch({
  type: SET_IS_UPDATE_SUCCESS,
  isSuccess
});

// setErrors
export const setValidationErrors = errors => async dispatch => dispatch({
  type: SET_VALIDATION_ERRORS,
  errors
});

export const setResetPasswordMessage = message => async dispatch => dispatch({
  type: SET_RESET_PWD_MESSAGE,
  message
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  try {
    const { user, token } = await authResponsePromise;
    setAuthData(user, token)(dispatch, getRootState);
  } catch (e) {
    dispatch(setValidationErrors(e));
  }
};

export const resetPasswordRequest = email => async dispatch => {
  try {
    const answer = await authService.resetPasswordRequest(email);
    dispatch(setResetPasswordMessage({ success: answer.message }));
  } catch (e) {
    dispatch(setResetPasswordMessage({ error: e.message }));
  }
};

export const resetPassword = request => async dispatch => {
  try {
    const answer = await authService.resetPassword(request);
    dispatch(setResetPasswordMessage({ success: answer.message }));
  } catch (e) {
    dispatch(setResetPasswordMessage({ error: e.message }));
  }
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const updateUserEmail = email => async (dispatch, getRootState) => {
  const currentUser = await getRootState().profile.user;
  try {
    const updatedEmail = await userService.updateUserEmail(currentUser.id, email);
    const updatedUser = () => ({
      ...currentUser,
      email: updatedEmail.value
    });
    dispatch(setUser(updatedUser()));
    dispatch(setUpdateSuccess({ isSuccess: 'true' }));
  } catch (e) {
    const error = ({
      email: e.value
    });
    dispatch(setValidationErrors(error));
  }
};

export const updateUserName = name => async (dispatch, getRootState) => {
  const currentUser = await getRootState().profile.user;
  try {
    const updatedName = await userService.updateUserName(currentUser.id, name);
    const updatedUser = () => ({
      ...currentUser,
      username: updatedName.value
    });
    dispatch(setUser(updatedUser()));
    dispatch(setUpdateSuccess({ isSuccess: 'true' }));
  } catch (e) {
    const error = ({
      username: e.value
    });
    dispatch(setValidationErrors(error));
  }
};

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const setUserImage = image => async (dispatch, getRootState) => {
  const currentUser = await getRootState().profile.user;
  const updatedUser = await userService.updateUserAvatar(currentUser.id, image);

  dispatch(setUser(updatedUser));
};

export const setUserStatus = status => async (dispatch, getRootState) => {
  const currentUser = await getRootState().profile.user;
  const updatedUser = await userService.updateUserStatus(currentUser.id, status);

  dispatch(setUser(updatedUser));
};
